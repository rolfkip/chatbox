from flask import Flask, request, session, g, redirect, url_for, abort, \
	render_template, flash
import os
from threading import Timer, Thread
from flask.ext.autoindex import AutoIndex

class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs={}, Verbose=None):
        Thread.__init__(self, group, target, name, args, kwargs, Verbose)
        self._return = None
    def run(self):
        if self._Thread__target is not None:
            self._return = self._Thread__target(*self._Thread__args,
                                                **self._Thread__kwargs)
    def join(self):
        Thread.join(self)
        return self._return

app = Flask('chatbox')
AutoIndex(app, browse_root=os.path.curdir)

@app.route('/chat', methods=['GET', 'POST'])
def chat_display():
	
	def readfile(name):
		chatlog = open(name, 'r')
		chat = chatlog.readlines()
		return chat
	
	if(request.args.get('name') != None):
		with open("chatlog.txt", "a") as chatlog:
			addtext = request.args.get('name') + ": " + request.args.get('line') + "\n"
			chatlog.write(addtext)
	# t = ThreadWithReturnValue(target=readfile, args=('chatlog.txt',))
	# t.start()
	# chat = t.join()
	debug = 0
	chat=[]
	with open("chatlog.txt", "r") as chatlog:
		for line in chatlog:
			chat.append(line)

	return render_template('chatbox.html', chat=chat, debug=debug)

if __name__ == '__main__':
	app.debug = True
	app.run(host='0.0.0.0', port=9020, threaded=True)
